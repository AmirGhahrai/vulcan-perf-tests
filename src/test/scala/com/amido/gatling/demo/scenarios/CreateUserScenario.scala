package com.amido.gatling.demo.scenarios

import com.amido.gatling.demo.requests.{CreateUserRequest, GetTokenRequest}
import io.gatling.core.Predef.scenario

object CreateUserScenario {
  val createUserScenario = scenario("Create User Scenario")
    .exec(GetTokenRequest.get_token)
    .exec(CreateUserRequest.create_user)
}
