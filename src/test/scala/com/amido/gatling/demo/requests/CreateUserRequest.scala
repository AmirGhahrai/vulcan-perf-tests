package com.amido.gatling.demo.requests

import com.amido.gatling.demo.config.Config.app_url
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object CreateUserRequest {

  val sentHeaders = Map("Authorization" -> "bearer ${token}")

  val usersFeeder = Array(
    Map("name" -> "Joe", "password" -> "MyCurrentPassword")).random

  val create_user = exec(http("Create User Request")
    .post(app_url + "/users")
    .headers(sentHeaders)
    .formParam("name", "${name}")
    .formParam("password", "${password}")
    .check(status is 201)
    .check(regex("Created").exists))
}
