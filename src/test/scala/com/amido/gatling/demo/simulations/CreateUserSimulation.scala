package com.amido.gatling.demo.simulations

import com.amido.gatling.demo.scenarios.CreateUserScenario
import io.gatling.core.Predef.Simulation
import io.gatling.core.Predef._
import com.amido.gatling.demo.config.Config._

class CreateUserSimulation extends Simulation {
  private val createUserExec = CreateUserScenario.createUserScenario
    .inject(atOnceUsers(users))

  setUp(createUserExec)
}
